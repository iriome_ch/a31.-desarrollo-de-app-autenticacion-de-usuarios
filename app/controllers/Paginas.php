<?php
//Page Controller
class Paginas extends Controller
{

    //Método para mostrar la página principal
    public function index()
    {
        if (isLoggedln()) {
            redirect('/posts/index');
        }
        $data = array(
            'titulo' => 'Framework de Iriome',
            'contenido' => 'Bienvenido al Framework de Iriome'
        );
        $this->view('paginas/index', $data, $active = 'home');
    }

    public function about()
    {
        $this->view('paginas/about', $data = [], $active = 'about');
    }
}
