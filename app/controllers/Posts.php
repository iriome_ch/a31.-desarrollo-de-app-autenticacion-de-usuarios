<?php

use \Tamtamchik\SimpleFlash\Flash;


class Posts extends Controller
{

    public function __construct()
    {
        if (!isLoggedln()) {
            redirect('/users/login');
        }
        $this->postModel = $this->model('Post');
        $this->userModel = $this->model('User');
        // $this->commentModel = $this->model('Comment');
    }

    public function index()
    {


        $data = [
            'posts' => $this->postModel->getPosts()
        ];

        $this->view('posts/index', $data, $active = 'home');
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['user_id'],
                // La variable $_FILES es un array asociativo que contiene los datos de cada archivo subido.
                'image' => !empty($_FILES['image']['name']) ? $_FILES['image']['name'] : '',
                'image_err' => '',
                'title_err' => '',
                'body_err' => ''
            ];

            // Validate title
            if (empty($data['title'])) {
                $data['title_err'] = 'Por favor ingrese un titulo';
            }

            // Validate body
            if (empty($data['body'])) {
                $data['body_err'] = 'Por favor ingrese un cuerpo';
            }

            // if ($_SERVER['Content_Length'] >= ini_get('post_max_size')) {
            //     $data['image_err'] = 'El archivo es demasiado grande';
            // };

            if (!empty($data['image'])) {
                // if ($_FILES['image']['error'] !== UPLOAD_ERR_OK) {
                //     // dd('error');
                //     // dd($_FILES['image']['error']);
                //     switch ($_FILES['image']['error']) {
                //         case UPLOAD_ERR_INI_SIZE:
                //         case UPLOAD_ERR_FORM_SIZE:
                //             $data['image_err'] = 'El fichero es demasiado grande. Tamaño máximo: ' . ini_get('upload_max_filesize');
                //             break;
                //         case UPLOAD_ERR_PARTIAL:
                //             $data['image_err'] = "No se ha podido subir el fichero completo";
                //             break;
                //         default:
                //             $data['image_err'] = "Error al subir el fichero";
                //             break;
                //     }
                // } else {
                //     // La variable $arrTypes contiene los tipos de archivos permitidos.
                //     $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                //     //La funcion in_array() comprueba si un elemento está en un array.
                //     if (in_array($_FILES['image']['type'], $arrTypes) === false) {
                //         $data['image_err'] = "Tipo de fichero no soportado";
                //     } else if (is_uploaded_file($_FILES['image']['tmp_name']) === false) {
                //         $data['image_err'] = "El archivo no se ha subido mediante un formulario";
                //     } else if (move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $_FILES['image']['name']) === false) {
                //         $data['image_err'] = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
                //     }
                // }

                try {

                    $file = new File($_FILES['image']);
                    $file->checkErrors();
                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            //dd($data['image_err']);
            // Make sure no errors
            if (empty($data['image_err']) && empty($data['title_err']) && empty($data['body_err'])) {
                // Validate
                if ($this->postModel->addPost($data)) {
                    $flash = new Flash();
                    $flash->message('Post agregado con exito', 'success');
                    redirect('/posts');
                } else {
                    die('Algo salio mal');
                }
            } else {
                // Load view with errors
                $this->view('posts/add', $data);
            }
        } else {
            $data = [
                'title' => '',
                'body' => '',
                'image' => '',
                'title_err' => '',
                'body_err' => '',
                'image_err' => ''
            ];

            $this->view('posts/add', $data);
        }
    }

    public function show($id)
    {
        $post = $this->postModel->getPostById($id);
        $user = $this->userModel->getUserById($post->user_id);

        $data = [
            'post' => $post,
            'user' => $user
        ];

        $this->view('posts/show', $data);
    }

    public function edit($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'id' => $id,
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['user_id'],
                'image' => !empty($_FILES['image']['name']) ? $_FILES['image']['name'] : '',
                'title_err' => '',
                'body_err' => '',
                'image_err' => ''
            ];

            // Validate title
            if (empty($data['title'])) {
                $data['title_err'] = 'Por favor ingrese un titulo';
            }

            // Validate body
            if (empty($data['body'])) {
                $data['body_err'] = 'Por favor ingrese un cuerpo';
            }

            if ($_SERVER['Content_Length'] >= ini_get('post_max_size')) {
                $data['image_err'] = 'El archivo es demasiado grande';
            };

            if (!empty($data['image'])) {
                if ($_FILES['image']['error'] !== UPLOAD_ERR_OK) {
                    switch ($_FILES['image']['error']) {
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            $data['image_err'] = 'El fichero es demasiado grande. Tamaño máximo: ' . ini_get('upload_max_filesize');
                            break;
                        case UPLOAD_ERR_PARTIAL:
                            $data['image_err'] = "No se ha podido subir el fichero completo";
                            break;
                        default:
                            $data['image_err'] = "Error al subir el fichero";
                            break;
                    }
                } else {
                    $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                    if (in_array($_FILES['image']['type'], $arrTypes) === false) {
                        $data['image_err'] = "Tipo de fichero no soportado";
                    } else if (is_uploaded_file($_FILES['image']['tmp_name']) === false) {
                        $data['image_err'] = "El archivo no se ha subido mediante un formulario";
                    } else if (move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $_FILES['image']['name']) === false) {
                        $data['image_err'] = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
                    }
                }
            }

            // Make sure no errors
            if (empty($data['title_err']) && empty($data['body_err'])) {
                // Validated
                if ($this->postModel->updatePost($data)) {
                    $flash = new Flash();
                    $flash->message('Post actualizado con exito', 'info');
                    redirect('/posts');
                } else {
                    die('Algo salio mal');
                }
            } else {
                // Load view with errors
                $this->view('/posts/edit', $data);
            }
        } else {

            // Get existing post from model
            $post = $this->postModel->getPostById($id);

            // Check for owner
            if ($post->user_id != $_SESSION['user_id']) {
                redirect('/posts');
            }

            $data = [
                'id' => $id,
                'title' => $post->title,
                'body' => $post->body,
                'image' => $post->image,
                'title_err' => '',
                'body_err' => '',
                'image_err' => ''
            ];

            $this->view('/posts/edit', $data);
        }
    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Get existing post from model
            $post = $this->postModel->getPostById($id);

            // Check for owner
            if ($post->user_id != $_SESSION['user_id']) {
                redirect('/posts');
            }

            if ($this->postModel->deletePost($id)) {
                $flash = new Flash();
                $flash->message('Post eliminado con exito', 'danger');
                redirect('/posts');
            } else {
                die('Algo salio mal');
            }
        } else {
            redirect('/posts');
        }
    }
}
