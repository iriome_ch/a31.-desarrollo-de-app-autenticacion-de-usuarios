<?php
//helper dd
function dd($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
    die();
}

function isLoggedln()
{
    if (!isset($_SESSION['user_id'])) {
        return false;
    } else {
        return true;
    }
}
