<?php
// helper para redireccionar a otra pagina de la aplicacion
function redirect($url)
{
    header("Location:" . URLROOT . "$url");
    exit;
}
