<?php
//FileException.php
//FileExeption class
class FileException extends Exception
{
    public function __construct($message)
    {
        // parent hace referencia a la clase padre
        parent::__construct($message);
    }
}
