<?php
//SIMPLE CONFIGURATION
define('APPROOT', dirname(dirname(__FILE__)));
define('URLROOT', 'http://192.168.56.103/a31.-desarrollo-de-app-autenticacion-de-usuarios');
define('SITENAME', 'Framework');


//DATABASE CONFIGURATION
define('DB_HOST', $_ENV['DB_HOST']);
define('DB_USER', $_ENV['DB_USER']);
define('DB_PASS', $_ENV['DB_PASS']);
define('DB_NAME', $_ENV['DB_NAME']);

//app version
define('APPVERSION', $_ENV['APPVERSION']);
