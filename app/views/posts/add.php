<?php include_once APPROOT . "/views/partials/header.php"; ?>
<a class="btn btn-warning pull-right" href="<?php echo URLROOT; ?>/posts" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<div class="card card-body bg-light mt-5">
    <h2>Crear publicación</h2>
    <p>Por favor introduzca los datos de su publicación</p>
    <form action="<?php echo URLROOT; ?>/posts/add" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
        <div class="form-group">
            <label for="title">Título:</label>
            <input type="text" name="title" class="form-control form-control-lg <?php echo (!empty($data['title_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['title']; ?>">
            <span class="invalid-feedback"><?php echo $data['title_err']; ?></span>
        </div>
        <div class="form-group">
            <label for="body">Contenido: </label>
            <textarea name="body" class="form-control from-control-lg <?php echo (!empty($data['body_err'])) ? 'is-invalid' : ''; ?>" rows="5" placeholder="Su contenido"> <?php echo $data['body']; ?></textarea>
            <span class="invalid-feedback"><?php echo $data['body_err']; ?></span>
        </div>
        <div class="form-group">
            <label for="image">Imagen:</label>
            <input type="file" name="image" class="form-control form-control-lg <?php echo (!empty($data['image_err'])) ? 'is-invalid' : ''; ?>">
            <span class="invalid-feedback"><?php echo $data['image_err']; ?></span>
        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="Crear publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>
<?php include_once APPROOT . "/views/partials/footer.php"; ?>