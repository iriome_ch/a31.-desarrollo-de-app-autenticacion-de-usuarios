<?php include_once APPROOT . "/views/partials/header.php"; ?>
<?php include_once APPROOT . "/views/partials/navbar.php"; ?>
<div class="flashes">
    <?= (string) flash() ?>
</div>
<div class="row mb-3">
    <div class="col-md-6">
        <h1>Publicaciones</h1>
    </div>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="<?= URLROOT  ?>/posts/add" role="button">
            <i class="fas fa-pencil-alt"></i> Crear publicación
        </a>
    </div>
</div>

<?php foreach ($data['posts'] as $post) { ?>
    <div class="row">
        <div class="col-md-4">
            <div class="card mb-4 box-shadow">

                <div class="card-body">
                    <?php if ($post->image) { ?>
                        <img src="<?= URLROOT ?>/public/img/<?= $post->image ?>" class="img-fluid" alt="" width="200" height="200">
                    <?php } ?>
                    <h2 class="card-title"><?= $post->title ?></h2>
                    <p class="card-text"><?= $post->body ?></p>
                    <div class="d-flex justify-content-between align-items-center">
                        <small class="text-muted">Creado por: <?= $post->name ?> el <?= $post->created_at ?> </small>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <a class="btn btn-primary" href="<?= URLROOT ?>/posts/show/<?= $post->postId ?>" role="button">
                            Ver
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php include_once APPROOT . "/views/partials/footer.php"; ?>