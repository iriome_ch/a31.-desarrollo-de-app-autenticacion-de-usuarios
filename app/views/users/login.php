<?php require APPROOT . '/views/partials/header.php'; ?>
<div class="flashes">
    <?= (string) flash() ?>
</div>
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">

            <h2>Iniciar sesión</h2>
            <p>Por favor, introduzca su correo y contraseña</p>
            <form action="<?php echo URLROOT; ?>/users/login" method="post" class="form-group needs-validation" novalidate>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email']; ?>" placeholder="Introduzca su email">
                    <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['password']; ?>" placeholder="Introduzca su contraseña">
                    <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="<?php echo URLROOT; ?>/users/register" class="btn btn-dark">Registrarse</a>
                    </div>
                    <div class="col">
                        <input type="submit" value="Iniciar sesión" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require APPROOT . '/views/partials/footer.php'; ?>