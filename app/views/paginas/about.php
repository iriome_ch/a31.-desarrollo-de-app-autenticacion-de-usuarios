<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>
<main>
    <div class="container">
        <!--- About Jumbotron -->
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">About</h1>
                <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                <p class="lead">It uses utility classes for typography and spacing to space content out within the larger container.</p>
                <p class="lead">Version: <i class="btn btn-info"><?= APPVERSION ?></i></p>
            </div>

        </div>
</main>