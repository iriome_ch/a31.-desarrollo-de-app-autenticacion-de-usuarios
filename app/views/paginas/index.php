<?php include_once APPROOT . "/views/partials/header.php"; ?>
<?php include_once APPROOT . "/views/partials/navbar.php"; ?>
<main>
    <div class="container py-4">
        <!--- Jumbotron -->
        <div class="jumbotron">
            <h1 class="display-4">Bienvenido a la aplicación!</h1>
            <p class="lead">Esta es una aplicación de ejemplo para el curso de PHP de Platzi.</p>
            <hr class="my-4">
            <p>
                <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
            </p>
        </div>
    </div>
</main>
<?php include_once APPROOT . "/views/partials/footer.php"; ?>