<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= SITENAME ?></title>

    <link rel="stylesheet" href="<?= URLROOT ?>/public/css/style.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/public/css/animate.min.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/public/css/ionicons.min.css">
</head>

<body>