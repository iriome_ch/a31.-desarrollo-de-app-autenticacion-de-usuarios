<nav id="topNav" class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i><?= SITENAME ?></a>
        </div>
        <div id="bs-navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a class="page-scroll  nav-link  <?= $active == 'home' ? 'active' : '' ?>" href="<?= URLROOT ?>/paginas/index">Home</a>
                </li>
                <li class="nav-item">
                    <a class="page-scroll  nav-link <?= $active == 'about' ? 'active' : '' ?>" href="<?= URLROOT ?>/paginas/about">About</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (isLoggedln() == false) { ?>
                    <li class="nav-item">
                        <a class="page-scroll  nav-link <?= $active == 'login' ? 'active' : '' ?>" href="<?= URLROOT ?>/users/login">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="page-scroll  nav-link <?= $active == 'register' ? 'active' : '' ?>" href="<?= URLROOT ?>/users/register">Register</a>
                    </li>
                <?php } else { ?>
                    <li class="nav-item">
                        <a class="page-scroll nav-link " href="#"><?php echo $_SESSION['user_name']; ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="page-scroll nav-link  <?= $active == 'logout' ? 'active' : '' ?>" href="<?= URLROOT ?>/users/logout">Logout</a>
                    </li>

                <?php } ?>
            </ul>
        </div>
    </div>
</nav>