<?php
class File
{
    protected $file;
    protected $types;
    public function __construct($file, $types = ['image/jpeg', 'image/png', 'image/gif'])
    {
        $this->file = $file;
        $this->types = $types;
    }

    public function typeError()
    {
        $type = $this->file['type'];
        if (!in_array($type, $this->types)) {
            throw new FileException('El archivo no es una imagen');
        }
    }

    public function sizeError()
    {
        if (filesize($this->file) > ini_get('upload_max_filesize')) {
            throw new FileException('El archivo es demasiado grande. Tamano maximo: ' . ini_get('upload_max_filesize'));
        }
    }

    public function uploadError()
    {
        if (is_uploaded_file($this->file = $_FILES['image']['tmp_name']) === false) {
            throw new FileException('El archivo no se ha podido subir');
        }
    }

    public function moveError()
    {
        if (move_uploaded_file($this->file = $_FILES['image']['tmp_name'], 'img/' . $_FILES['image']['name']) === false) {
            throw new FileException('Ocurrió algún error al subir el fichero. No pudo guardarse');
        }
    }

    public function partialError()
    {
        if ($this->file['error'] !== UPLOAD_ERR_PARTIAL) {
            throw new FileException('El archivo no se ha podido subir');
        }
    }

    public function checkErrors()
    {
        if ($this->file['error'] !== UPLOAD_ERR_OK) {


            $this->sizeError();

            $this->partialError();
        }
        $this->typeError();
        $this->uploadError();
        $this->moveError();
    }
}
